-- Create a table for channels
create table channels (
  id uuid not null primary key,
  name text unique,

  constraint name_length check (char_length(name) >= 3)
);

-- Create a table for messages
create table chats (
  id uuid not null primary key,
  user_id uuid references auth.users (id) on delete cascade not null,
  channel_id uuid references channels (id) on delete cascade not null,
  created_at timestamp with time zone,
  content text
);
-- Set up Row Level Security (RLS)
-- See https://supabase.com/docs/guides/auth/row-level-security for more details.
alter table chats
  enable row level security;

create policy "Users can insert their own chats." on chats
  for all using (auth.uid() = user_id) with check (auth.uid() = user_id);

alter
  publication supabase_realtime add table chats;

