// import 'package:supabase/supabase.dart';
// import 'package:uuid/uuid.dart';

// class SupabaseService {
//   static SupabaseClient? _supabaseClient;

//   static SupabaseClient get supabaseClient {
//     if (_supabaseClient == null) {
//       const url = ''; // TODO: Get URL from environment variable
//       const key = ''; // TODO: Get key from environment variable
//       _supabaseClient = SupabaseClient(url, key);
//     }
//     return _supabaseClient!;
//   }
// }

// class User {
//   final String id;
//   final String name;
//   final String email;

//   User({
//     required this.id,
//     required this.name,
//     required this.email,
//   });

//   Map<String, dynamic> toJson() {
//     return {
//       'id': id,
//       'name': name,
//       'email': email,
//     };
//   }

//   factory User.fromJson(Map<String, dynamic> json) {
//     return User(
//       id: json['id'],
//       name: json['name'],
//       email: json['email'],
//     );
//   }
// }

// class UserChat {
//   final int id;
//   final int userId;
//   final String message;

//   UserChat({
//     required this.id,
//     required this.userId,
//     required this.message,
//   });

//   Map<String, dynamic> toJson() {
//     return {
//       'id': id,
//       'userId': userId,
//       'message': message,
//     };
//   }

//   factory UserChat.fromJson(Map<String, dynamic> json) {
//     return UserChat(
//       id: json['id'],
//       userId: json['userId'],
//       message: json['message'],
//     );
//   }
// }

// SupabaseQueryBuilder _getUsersQuery() {
//   return SupabaseService.supabaseClient.from('users');
// }

// class UserRepository {
//   Future<User> getUser(int id) async {
//     try {
//       return SupabaseService.supabaseClient
//           .from('users')
//           .select()
//           .eq('id', id)
//           .single()
//           .withConverter<User>((data) => User.fromJson(data));
//     } catch (e) {
//       throw Exception('Failed to fetch user: $e');
//     }
//   }

//   Future<List<User>> getUsers() async {
//     try {
//       return SupabaseService.supabaseClient
//           .from('users')
//           .select<PostgrestList>()
//           .withConverter((data) => data.map((e) => User.fromJson(e)).toList());
//     } catch (e) {
//       throw Exception('Failed to fetch users: $e');
//     }
//   }

//   Future<void> createUser(User user) async {
//     try {
//       final response = await SupabaseService.supabaseClient
//           .from('users')
//           .insert(user.toJson())
//           .execute();
//       if (response.error != null) {
//         throw Exception('Failed to create user: ${response.error!.message}');
//       }
//     } catch (e) {
//       throw Exception('Failed to create user: $e');
//     }
//   }

//   Future<void> updateUser(User user) async {
//     try {
//       final response = await SupabaseService.supabaseClient
//           .from('users')
//           .update(user.toJson())
//           .eq('id', user.id)
//           .execute();
//       if (response.error != null) {
//         throw Exception('Failed to update user: ${response.error!.message}');
//       }
//     } catch (e) {
//       throw Exception('Failed to update user: $e');
//     }
//   }

//   Future<void> deleteUser(int id) async {
//     try {
//       final response = await SupabaseService.supabaseClient
//           .from('users')
//           .delete()
//           .eq('id', id)
//           .execute();
//       if (response.error != null) {
//         throw Exception('Failed to delete user: ${response.error!.message}');
//       }
//     } catch (e) {
//       throw Exception('Failed to delete user: $e');
//     }
//   }
// }

// class UserChatRepository {
//   Future<UserChat> getUserChat(int id) async {
//     try {
//       final response = await SupabaseService.supabaseClient
//           .from('user_chats')
//           .select()
//           .eq('id', id)
//           .single()
//           .execute();
//       if (response.error != null) {
//         throw Exception(
//             'Failed to fetch user chat: ${response.error!.message}');
//       }
//       final data = response.data;
//       if (data == null) {
//         throw Exception('User chat not found');
//       }
//       return UserChat(
//         id: data['id'],
//         userId: data['userId'],
//         message: data['message'],
//       );
//     } catch (e) {
//       throw Exception('Failed to fetch user chat: $e');
//     }
//   }

//   Future<List<UserChat>> getUserChats() async {
//     try {
//       final response = await SupabaseService.supabaseClient
//           .from('user_chats')
//           .select()
//           .execute();
//       if (response.error != null) {
//         throw Exception(
//             'Failed to fetch user chats: ${response.error!.message}');
//       }
//       final data = response.data;
//       if (data == null) {
//         throw Exception('No user chats found');
//       }
//       return data
//           .map((item) => UserChat(
//                 id: item['id'],
//                 userId: item['userId'],
//                 message: item['message'],
//               ))
//           .toList();
//     } catch (e) {
//       throw Exception('Failed to fetch user chats: $e');
//     }
//   }

//   Future<void> createUserChat(UserChat userChat) async {
//     try {
//       final response = await SupabaseService.supabaseClient
//           .from('user_chats')
//           .insert(userChat.toJson())
//           .execute();
//       if (response.error != null) {
//         throw Exception(
//             'Failed to create user chat: ${response.error!.message}');
//       }
//     } catch (e) {
//       throw Exception('Failed to create user chat: $e');
//     }
//   }

//   Future<void> updateUserChat(UserChat userChat) async {
//     try {
//       final response = await SupabaseService.supabaseClient
//           .from('user_chats')
//           .update(userChat.toJson())
//           .eq('id', userChat.id)
//           .execute();
//       if (response.error != null) {
//         throw Exception(
//             'Failed to update user chat: ${response.error!.message}');
//       }
//     } catch (e) {
//       throw Exception('Failed to update user chat: $e');
//     }
//   }

//   Future<void> deleteUserChat(int id) async {
//     try {
//       final response = await SupabaseService.supabaseClient
//           .from('user_chats')
//           .delete()
//           .eq('id', id)
//           .execute();
//       if (response.error != null) {
//         throw Exception(
//             'Failed to delete user chat: ${response.error!.message}');
//       }
//     } catch (e) {
//       throw Exception('Failed to delete user chat: $e');
//     }
//   }
// }
