import 'package:flutter/material.dart';
import 'package:flutter_demo/src/supabase/supabase.dart';
import 'package:flutter_demo/src/ui/chat_entry.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:flutter_demo/src/app_drawer.dart';
import 'package:uuid/uuid.dart';

class User {
  final String id;
  final String username;
  final String avatarUrl;

  const User({
    required this.id,
    required this.username,
    required this.avatarUrl,
  });
}

class ChannelPage extends StatefulWidget {
  final String channelName;
  final String channelDescription;

  const ChannelPage(
      {super.key, required this.channelName, required this.channelDescription});

  static const routeName = '/channel';

  @override
  _ChannelPageState createState() => _ChannelPageState();
}

class _ChannelPageState extends State<ChannelPage> {
  final _inputController = TextEditingController();

  var _loading = true;
  var _channel = <String, dynamic>{};
  var _messages = <Map<String, dynamic>>[];
  var _users = Map<String, User>();

  Future<User> _getUser(String userId) async {
    if (!_users.containsKey(userId)) {
      final data = await supabase
          .from('profiles')
          .select('username, avatar_url')
          .eq('id', userId)
          .single();
      final user = User(
        id: userId,
        username: data['username'] as String,
        avatarUrl: data['avatar_url'] ?? '',
      );

      _users[userId] = user;
    }

    return _users[userId]!;
  }

  /// Called once a user id is received within `onAuthenticated()`
  Future<void> _getChannel() async {
    setState(() {
      _loading = true;
    });

    try {
      _channel = await supabase
          .from('channels')
          .select<Map<String, dynamic>>()
          .eq('name', widget.channelName)
          .single();
    } on PostgrestException catch (error) {
      SnackBar(
        content: Text(error.message),
        backgroundColor: Theme.of(context).colorScheme.error,
      );
    } catch (error) {
      SnackBar(
        content: const Text('Unexpected error occurred'),
        backgroundColor: Theme.of(context).colorScheme.error,
      );
    } finally {
      if (mounted) {
        setState(() {
          _loading = false;
        });
      }
    }
  }

  Future<void> _listenToChats() async {
    supabase
        .channel('chats')
        .on(
          RealtimeListenTypes.postgresChanges,
          ChannelFilter(
            event: 'INSERT',
            schema: 'public',
            table: 'chats',
            // filter: 'channel_id=eq.${_channel['id']}',
          ),
          (payload, [ref]) async => {
            print('ADD MESSAGE $payload ${ref.toString()}'),
            await _getUser(payload['new']['user_id']),
            print('users'),
            print(_users.toString()),
            setState(() {
              _messages.add(payload['new']);
              _inputController.clear();
            }),
          },
        )
        .subscribe(
          (status, [error]) => print('dupa ${status}'),
        );
  }

  /// Called when user taps `Update` button
  Future<void> _updateProfile() async {
    setState(() {
      _loading = true;
    });
    final userName = _inputController.text.trim();
    final user = supabase.auth.currentUser;
    final updates = {
      'id': user!.id,
      'username': userName,
      'updated_at': DateTime.now().toIso8601String(),
    };
    try {
      await supabase.from('profiles').upsert(updates);
      if (mounted) {
        const SnackBar(
          content: Text('Successfully updated profile!'),
        );
      }
    } on PostgrestException catch (error) {
      SnackBar(
        content: Text(error.message),
        backgroundColor: Theme.of(context).colorScheme.error,
      );
    } catch (error) {
      SnackBar(
        content: const Text('Unexpected error occurred'),
        backgroundColor: Theme.of(context).colorScheme.error,
      );
    } finally {
      if (mounted) {
        setState(() {
          _loading = false;
        });
      }
    }
  }

  Future<void> _sendChat() async {
    print('send chat');
    final message = _inputController.text.trim();
    final user = supabase.auth.currentUser;
    final uuid = Uuid();
    final updates = {
      'id': uuid.v4(),
      'channel_id': _channel['id'],
      'user_id': user!.id,
      'content': message,
      'created_at': DateTime.now().toIso8601String(),
    };
    try {
      print('try send chat ${updates.toString()}');
      var row = await supabase.from('chats').insert(updates).select();

      print('sent chat ${row.toString()}');
      if (mounted) {
        const SnackBar(
          content: Text('Successfully sent message!'),
        );
      }
    } on PostgrestException catch (error) {
      print('error sending chat ${error.message}');
      SnackBar(
        content: Text(error.message),
        backgroundColor: Theme.of(context).colorScheme.error,
      );
    } catch (error) {
      print('error sending chat ${error.toString()}');
      SnackBar(
        content: const Text('Unexpected error occurred'),
        backgroundColor: Theme.of(context).colorScheme.error,
      );
    } finally {
      if (mounted) {
        setState(() {
          _loading = false;
        });
      }
    }
  }

  Future<void> _signOut() async {
    try {
      await supabase.auth.signOut();
    } on AuthException catch (error) {
      SnackBar(
        content: Text(error.message),
        backgroundColor: Theme.of(context).colorScheme.error,
      );
    } catch (error) {
      SnackBar(
        content: const Text('Unexpected error occurred'),
        backgroundColor: Theme.of(context).colorScheme.error,
      );
    } finally {
      if (mounted) {
        Navigator.of(context).pushReplacementNamed('/login');
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _getChannel();
    _listenToChats();
  }

  @override
  void dispose() async {
    _inputController.dispose();
    await supabase.removeAllChannels();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('#${widget.channelName}')),
      drawer: AppDrawer(),
      body: _loading
          ? const Center(child: CircularProgressIndicator())
          : Column(
              children: [
                Flexible(
                  child: ListView.builder(
                    padding: const EdgeInsets.symmetric(
                      vertical: 18,
                      horizontal: 12,
                    ),
                    itemCount: _messages.length,
                    itemBuilder: (context, index) => ChatEntryWidget(
                      user: _users[_messages[index]['user_id']] ??
                          User(
                            id: _messages[index]['user_id'],
                            username: 'Unknown',
                            avatarUrl: '',
                          ),
                      content: _messages[index]['content'],
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 24),
                  child: Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          controller: _inputController,
                          decoration: const InputDecoration(
                            labelText: 'Message',
                            hintText: 'Enter your message',
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: _sendChat,
                        icon: const Icon(Icons.send),
                      ),
                    ],
                  ),
                ),
              ],
            ),
    );
  }
}
