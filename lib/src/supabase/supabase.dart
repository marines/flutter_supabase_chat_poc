import 'package:supabase_flutter/supabase_flutter.dart';

Future<Supabase> initializeSupabaseClient() async {
  const supabaseUrl = 'https://xqxsozpcqrepldzehuoj.supabase.co';
  const supabaseKey =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InhxeHNvenBjcXJlcGxkemVodW9qIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDIyMzk1NjEsImV4cCI6MjAxNzgxNTU2MX0.o-3EAZQhiv8P3Ex-NT0MzLkrnsw-ExcbctNSLUZmmFU';

  final client = Supabase.initialize(
    url: supabaseUrl,
    anonKey: supabaseKey,
    authFlowType: AuthFlowType.pkce,
    realtimeClientOptions: const RealtimeClientOptions(
      eventsPerSecond: 2,
    ),
  );

  return client;
}

final supabase = Supabase.instance.client;
