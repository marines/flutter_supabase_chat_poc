import 'package:flutter/material.dart';
import 'package:flutter_demo/src/pages/channel_page.dart';

class ChatEntryWidget extends StatelessWidget {
  final User user;
  final String content;

  const ChatEntryWidget({
    super.key,
    required this.user,
    required this.content,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
        visualDensity: VisualDensity.compact,
        leading: CircleAvatar(
          backgroundColor: Colors.transparent,
          child: SizedBox(
            width: 24,
            height: 24,
            child: ClipOval(
              child: user.avatarUrl.isNotEmpty
                  ? Image.network(
                      user.avatarUrl,
                      fit: BoxFit.cover,
                    )
                  : Image.asset(
                      'assets/images/default_avatar.jpg',
                      fit: BoxFit.cover,
                    ),
            ),
          ),
        ),
        title: RichText(
          text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              children: <TextSpan>[
                TextSpan(
                    text: '${user.username}:',
                    style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: ' '),
                TextSpan(text: content),
              ]),
        ));
  }
}
